# test_grpc_etcd
该项目用于演示使用etcd作为服务注册中心来对grpc服务进行服务注册与服务发现

## grpc相关
[参考](https://www.liwenzhou.com/posts/Go/gRPC/)

`protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative pb/hello.proto`


安装相关的库
*注意使用*
`go get go.etcd.io/etcd/client/v3`

而不要使用`go get go.etcd.io/etcd/clientv3`

之前的clientv3和grpc的版本高版本不兼容，有很多的问题，很多老的博客都还是使用的clientv3
QuickStart见[QuickStart](https://github.com/etcd-io/etcd/blob/main/client/v3/README.md)


## 如何使用
1. ./master
2. ./server -port="9081" -name="hello1"
3. ./server -port="9082" -name="hello2"
3. ./client -addr=127.0.0.1:9081 -name=ppyang
