package discovery

import (
	"context"
	"encoding/json"
	"fmt"
	clientv3 "go.etcd.io/etcd/client/v3"
	"log"
	"time"
)

type Master struct {
	Path   string
	Nodes  map[string]*Node
	Client *clientv3.Client
}

type Node struct {
	State bool
	Key   string
	Info  ServiceInfo
}

func NewMaster(endpoints []string, watchPath string) (*Master, error) {
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: time.Second,
	})
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	master := &Master{
		Path:   watchPath,
		Nodes:  make(map[string]*Node),
		Client: cli,
	}

	go master.WatchNodes()

	return master, nil
}

// 添加Node
func (m *Master) AddNode(key string, info *ServiceInfo) {
	node := &Node{
		State: true,
		Key:   key,
		Info:  *info,
	}
	m.Nodes[key] = node
}

func GetServiceInfo(ev *clientv3.Event) *ServiceInfo {
	info := &ServiceInfo{}
	err := json.Unmarshal([]byte(ev.Kv.Value), info)
	if err != nil {
		log.Println(err)
	}
	return info
}

// 监控节点
func (m *Master) WatchNodes() {
	wch := m.Client.Watch(context.Background(), m.Path, clientv3.WithPrefix())
	for wrsp := range wch {
		for _, event := range wrsp.Events {
			switch event.Type {
			case clientv3.EventTypePut:
				fmt.Printf("[%s] %q:%q", event.Type, event.Kv.Key, event.Kv.Value)
				info := GetServiceInfo(event)
				m.AddNode(string(event.Kv.Key), info)
			case clientv3.EventTypeDelete:
				fmt.Printf("[%s] %q:%q", event.Type, event.Kv.Key, event.Kv.Value)
				delete(m.Nodes, string(event.Kv.Key))
			}
		}
	}
}
