package main

import (
	"fmt"
	"log"
	"test_grpc_etcd/discovery"
	"time"
)

func main() {
	m, err := discovery.NewMaster([]string{
		"127.0.0.1:2379",
		"127.0.0.1:22379",
		"127.0.0.1:32379",
	}, "hello")

	if err != nil {
		log.Fatal(err)
	}

	for {
		for k, v := range m.Nodes {
			fmt.Printf("node:%s, ip=%s\n", k, v.Info.ServerAddr)
		}
		fmt.Printf("nodes num = %d\n", len(m.Nodes))
		time.Sleep(time.Second * 5)
	}
}
