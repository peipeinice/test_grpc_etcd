package main

import (
	"context"
	"flag"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"test_grpc_etcd/discovery"
	"test_grpc_etcd/pb"
)

var (
	serviceIp   = flag.String("ip", "127.0.0.1", "set service ip")
	servicePort = flag.String("port", "80", "set service port")
	serviceName = flag.String("name", "hello", "set service name")
)

// hello server

type server struct {
	pb.UnimplementedGreeterServer
}

func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloResponse, error) {
	return &pb.HelloResponse{Reply: "Hello " + in.Name + "from: " + "hello/" + *serviceName}, nil
}

func main() {
	flag.Parse()
	url := fmt.Sprintf("%s:%s", *serviceIp, *servicePort)
	fmt.Println(url)
	lis, err := net.Listen("tcp", url)

	if err != nil {
		fmt.Printf("failed to listen: %v", err)
		return
	}

	s := grpc.NewServer()                  // 创建gRPC服务器
	pb.RegisterGreeterServer(s, &server{}) // 在gRPC服务端注册服务
	go registerToEtcd()
	// 启动服务
	err = s.Serve(lis)
	if err != nil {
		fmt.Printf("failed to serve: %v", err)
		return
	}

}

func registerToEtcd() {
	//注册到etcd
	serviceInfo := discovery.ServiceInfo{fmt.Sprintf("%s:%s", *serviceIp, *servicePort)}
	ser, err := discovery.NewService(*serviceName, "hello", serviceInfo, []string{
		"127.0.0.1:2379",
		"127.0.0.1:22379",
		"127.0.0.1:32379",
	})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("register service: %s:%s\n", *serviceIp, *servicePort)
	//go func() {
	//	time.Sleep(time.Second * 20)
	//	ser.Stop()
	//}()
	ser.Start()
}
